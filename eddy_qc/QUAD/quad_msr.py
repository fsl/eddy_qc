#!/usr/bin/env fslpython

import numpy as np
from pylab import MaxNLocator
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn
seaborn.set()





#=========================================================================================
# QUAD - Contrast to noise ratio (CNR) and mean squared residuals (MSR)
# Matteo Bastiani
# 01-06-2017, FMRIB, Oxford
#=========================================================================================

def main(pdf, data, eddy):
    """
    Generate page of the single subject report pdf that contains:
    - Per-shell average CNR bar plots (error bars are standard deviations).
    - Per-volume mean squared residuals plots (one plot for each b-value, including 0). Outliers
    (MSR > mean + std for each shell) are marked as red stars with the number of corresponding volume
    (0-based) next to them.
    
    Arguments:
        - pdf: qc pdf file
        - data: data dictionary containg information about the dataset
        - eddy: EDDY dictionary containg useful qc information
    """
    #================================================
    # Prepare figure
    fig = plt.figure(figsize=(8.27,11.69))   # Standard portrait A4 sizes
    plt.suptitle('Subject ' + data['subj_id'], fontsize=10, fontweight='bold')

    # Create plot for each b-shell including b=0
    axes = fig.subplots(1+data['unique_bvals'].size, 1, sharex=True)
    x = np.arange(data['bvals'].size)
    ax_b0 = axes[0]
    tmp_rss = eddy['avg_rss'][np.abs(data['bvals']) <= 100]
    mask_b0 = np.abs(data['bvals']) <= 100
    x_rss = x[mask_b0]
    ax_b0.plot(x_rss, tmp_rss, 'bo', label="b=0")
    outlier_mask = tmp_rss > (np.mean(tmp_rss) + 2*np.std(tmp_rss))
    if outlier_mask.any():
        ax_b0.plot(x_rss[outlier_mask], tmp_rss[outlier_mask], 'ro', label='Outliers')
        for idx, rss in zip(x_rss[outlier_mask], tmp_rss[outlier_mask]):
            ax_b0.annotate(str(idx), (idx, rss), xytext=(3, 3), textcoords='offset points')
    ol_vols = x_rss[outlier_mask]
    ax_b0.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax_b0.set_xlim(-0.5, data['bvals'].size - 0.5)
    ax_b0.set_ylabel("MSR")
    ax_b0.legend(loc='best', frameon=True, framealpha=0.5, numpoints=1)
    ax_b0.set_title("Mean squared residuals (MSR)")

    for i, ax in enumerate(axes[1:]):
        tmp_rss = eddy['avg_rss'][np.abs(data['bvals']-data['unique_bvals'][i]) <= 100]
        x_rss = x[np.abs(data['bvals']-data['unique_bvals'][i]) <= 100]
        outlier_mask = tmp_rss > (np.mean(tmp_rss) + 2*np.std(tmp_rss))
        ax.set_ylabel("MSR")
        ax.plot(x_rss, tmp_rss, 'bo', label="b=%d" % data['unique_bvals'][i])
        if outlier_mask.any():
            ax.plot(x_rss[outlier_mask], tmp_rss[outlier_mask], 'ro', label='Outliers')
            for idx, rss in zip(x_rss[outlier_mask], tmp_rss[outlier_mask]):
                ax.annotate(str(idx), (idx, rss), xytext=(3, 3), textcoords='offset points')
        ax.legend(loc='best', frameon=True, framealpha=0.5, numpoints=1)
        ol_vols = np.append(ol_vols, x_rss[outlier_mask])

    ax.set_xlabel("Volume")
    fig.tight_layout()

    # Format figure, save and close it
    plt.savefig(pdf, format='pdf')
    plt.close()
