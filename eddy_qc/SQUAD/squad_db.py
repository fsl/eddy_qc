#!/usr/bin/env fslpython

import json
import os
import numpy as np



#=========================================================================================
# SQUAD - Perform database I/O operations
# Matteo Bastiani
# 01-08-2017, FMRIB, Oxford
#=========================================================================================

def main(fn, op, sList):
    """
    Perform a database I/O operation:
    - read from .json file
    - write to .json file
    
    Arguments:
        - db: database in dictionary format
        - fn: filename
        - op: 'r' to read, 'w' to write
    """
    if op == 'w':
        
        countSubjects = 0
        data_list = []
        qc_mot_list = []
        qc_params_list = []
        qc_s2v_params_list = []
        qc_susc_list = []
        qc_ol_list = []
        qc_cnr_list = []

        ref_data = None
        
        with open(sList) as fp:
            for line in fp:
                line=line.rstrip('\n')
                qc_json = line + '/qc.json'
                if not os.path.isfile(qc_json):
                    raise ValueError(qc_json + ' does not appear to be a valid qc.json file')
                countSubjects = countSubjects + 1
                with open(qc_json) as qc_file:    
                    sData = json.load(qc_file)
                if ref_data is None:
                    ref_data = sData

                # Check that eddy has been run in the same way for all subjects
                for flag, description in [
                    ('qc_ol_flag', 'outlier deteciton'),
                    ('qc_field_flag', 'susceptibility field'),
                    ('qc_params_flag', 'topup acquisition parameeters'),
                    ('qc_s2v_params_flag', 'slice-to-volume correction'),
                    ('qc_cnr_flag', 'CNR output maps'),
                    ('qc_rss_flag', 'residual maps'),
                ]:
                    if ref_data[flag] != sData[flag]:
                        raise ValueError("Inconsistency detected in how eddy was run in {} flag!".format(description))

                # Check that data acquisition was consistent across all subjects

                for flag, description in [
                    ('data_no_shells', "number of shells"),
                    ('data_no_PE_dirs', "number of phase encode directions"),
                    ('data_no_b0_vols', "number of b0 volumes"),
                    ('data_no_dw_vols', "number of diffusion-weighted volumes"),
                    ('data_eddy_para', "topup acquisition parameters"),
                ]:
                    if sData[flag] != ref_data[flag]:
                        raise ValueError("Inconsistency detected in eddy input data in {}!".format(description))

                if (
                    len(sData['data_unique_bvals']) != len(ref_data['data_unique_bvals']) or 
                    not np.allclose(sData['data_unique_bvals'], ref_data['data_unique_bvals'], atol=20)
                    ):
                    raise ValueError("Inconsistency detected in b-values of eddy input data!")

                if (
                    not np.allclose(sData['data_vox_size'], ref_data['data_vox_size'], rtol=1e-2)
                    ):
                    raise ValueError("Inconsistency detected in voxel sizes of eddy input data!")

                # Initialize empty subject lists
                data_tmp = []
                mot_tmp = []
                par_tmp = []
                s2v_par_tmp = []
                susc_tmp = []
                ol_tmp = []
                cnr_tmp = []    

                # Store qc indices in lists
                data_tmp.extend(sData['data_protocol'])
                
                mot_tmp.extend([sData['qc_mot_abs'], sData['qc_mot_rel']])
                if sData['qc_params_flag']:
                    par_tmp.extend(sData['qc_params_avg'])
                if sData['qc_s2v_params_flag']:
                    s2v_par_tmp.extend(sData['qc_s2v_params_avg_std'])
                if sData['qc_field_flag']:
                    susc_tmp.append(sData['qc_vox_displ_std'])
                if sData['qc_ol_flag']:
                    ol_tmp.append(sData['qc_outliers_tot'])
                    ol_tmp.extend(sData['qc_outliers_b'])
                    ol_tmp.extend(sData['qc_outliers_pe'])
                if sData['qc_cnr_flag']:
                    cnr_tmp.extend(sData['qc_cnr_avg'])
                
                data_list.append(data_tmp)
                qc_mot_list.append(mot_tmp)
                qc_params_list.append(par_tmp)
                qc_s2v_params_list.append(s2v_par_tmp)
                qc_susc_list.append(susc_tmp)
                qc_ol_list.append(ol_tmp)
                qc_cnr_list.append(cnr_tmp)
        
        
        #=========================================================================================
        # Database creation as a dictionary
        #=========================================================================================       
        db = {
            # eddy options
            'ol_flag':ref_data['qc_ol_flag'],
            'par_flag':ref_data['qc_params_flag'],
            's2v_par_flag':ref_data['qc_s2v_params_flag'],
            'susc_flag':ref_data['qc_field_flag'],
            'cnr_flag':ref_data['qc_cnr_flag'],
            'rss_flag':ref_data['qc_rss_flag'],

            # data info
            'data_no_subjects':countSubjects,
            'data_no_shells':sData['data_no_shells'],
            'data_no_pes':sData['data_no_PE_dirs'],
            'data_no_b0_vols':sData['data_no_b0_vols'],
            'data_no_dw_vols':sData['data_no_dw_vols'],
            'data_unique_bvals':sData['data_unique_bvals'],
            'data_unique_pes':sData['data_eddy_para'],
            'data_protocol':data_list,
            'data_vox_size':sData['data_vox_size'],

            # qc indices
            'qc_motion':qc_mot_list,
            'qc_parameters':qc_params_list,
            'qc_s2v_parameters':qc_s2v_params_list,
            'qc_susceptibility':qc_susc_list,
            'qc_outliers':qc_ol_list,
            'qc_cnr':qc_cnr_list,
        }

        with open(fn, 'w') as fp:
            json.dump(db, fp, sort_keys=True, indent=4, separators=(',', ': '))
    
        return db

    elif op == 'r':
        if not os.path.isfile(fn):
            raise ValueError(fn + ' does not appear to be a valid group_db.json file')
        with open(fn, 'r') as fp:
            return json.load(fp)